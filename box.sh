#!/bin/bash

SRC=$HOME
DST=fsunrise.ru:/home/alexey
FOLDER=box

LAST_SYNC_TIME=0
LAST_MODIFIED_TIME=`date +%s`
let MINIMAL_SYNC_INTERVAL=20
let MINIMAL_REMOTE_SYNC_INTERVAL=10*60
UNISON_PROFILE="fs"
#TODO: read unison manual and pick up correct options
UNISON_OPTIONS="-auto -batch -ui text"

trap "killchild" SIGTERM SIGINT

function main
{
	sync
	monitor_remote_changes &
	REMOTE_MONITOR_PID=$!
	monitor_local_changes
	echo Exiting
}


function killchild 
{
	echo Stopping child processes...
	kill -TERM $REMOTE_MONITOR_PID
	wait $REMOTE_MONITOR_PID
	echo done
}

function monitor_local_changes 
{
	echo Starting monitoring for local changes...
	inotifywait -r -m -e modify -e move -e create -e delete $SRC/$FOLDER | while read ev ; do
	IFS=' ' read -a event <<< "$ev"
	echo event occured:\n
	echo folder to sync ${event[0]}
	echo operation ${event[1]}
	echo file ${event[2]}
	update_modified_time
	sync
done
}

function monitor_remote_changes
{
	echo Starting monitoring for remote changes...
	while true; do
		sleep $MINIMAL_REMOTE_SYNC_INTERVAL
		sync
	done
}

function sync 
{
	echo Syncing...
	let local time_from_last_sync=`date +%s`-$LAST_SYNC_TIME
	echo time from last sync $time_from_last_sync
	if [ "$time_from_last_sync" -lt "$MINIMAL_SYNC_INTERVAL" ]; then
		echo too frequently changes
	else
		call_unison
		update_sync_time
	fi
}

function call_unison
{
	unison $UNISON_PROFILE $UNISON_OPTIONS
}

function update_sync_time 
{
	echo Updating last sync time
	LAST_SYNC_TIME=`date +%s`
}

function update_modified_time 
{
	echo Updating last modified time
	LAST_MODIFIED_TIME=`date +%s`
}

main
